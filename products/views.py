from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from .models import Product
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404
from .forms import ProductForm
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import UserCreationForm
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from .mixins import LoginRequiredMixin


class ProductList(ListView):
	model = Product

class ProductDetail(DetailView):
	model = Product

@login_required()
def new_product(request):
	if request.method == 'POST':
		form = ProductForm(request.POST, request.FILES)
		if form.is_valid():
			product = form.save(commit=False)
			product.save()
			return HttpResponseRedirect('/')
	else:
		form = ProductForm()

	template = loader.get_template('new_product.html')
	context = {
		'form': form
	}
	return HttpResponse(template.render(context, request))








