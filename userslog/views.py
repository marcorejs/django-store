from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.http import HttpResponse, HttpResponseRedirect

# Create your views here.
def authentication(request):
	if request.method == 'POST':
			action = request.POST.get('action', None)
			username = request.POST.get('username', None)
			password = request.POST.get('password', None)
			if action == 'login':	
			    user = authenticate(username=username, password=password)
			    login(request, user)
			return redirect('/') 

	return render(request, 'login.html', {})